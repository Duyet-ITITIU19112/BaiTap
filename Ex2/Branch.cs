using System.ComponentModel.DataAnnotations;

namespace Ex2.Models
{
    public class Branch
    {
        public int BranchId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        public string ZipCode { get; set; }
    }
}

