using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace YourNamespace
{
    public class Branch
    {
        public int BranchId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
    }

    [Route("api/[controller]")]
    [ApiController]
    public class BranchesController : ControllerBase
    {
        private List<Branch> _branches;

        public BranchesController()
        {
            _branches = new List<Branch>();
        }

        // GET: api/branches
        [HttpGet]
        public ActionResult<IEnumerable<Branch>> Get()
        {
            return _branches;
        }

        // GET: api/branches/{id}
        [HttpGet("{id}")]
        public ActionResult<Branch> Get(int id)
        {
            var branch = _branches.Find(b => b.BranchId == id);
            if (branch == null)
                return NotFound();

            return branch;
        }

        // POST: api/branches
        [HttpPost]
        public ActionResult<Branch> Add(Branch branch)
        {
            _branches.Add(branch);
            return branch;
        }

        // PUT: api/branches/{id}
        [HttpPut("{id}")]
        public ActionResult<Branch> Edit(int id, Branch updatedBranch)
        {
            var branch = _branches.Find(b => b.BranchId == id);
            if (branch == null)
                return NotFound();

            branch.Name = updatedBranch.Name;
            branch.Address = updatedBranch.Address;
            branch.City = updatedBranch.City;
            branch.State = updatedBranch.State;
            branch.ZipCode = updatedBranch.ZipCode;

            return branch;
        }

        // DELETE: api/branches/{id}
        [HttpDelete("{id}")]
        public ActionResult Remove(int id)
        {
            var branch = _branches.Find(b => b.BranchId == id);
            if (branch == null)
                return NotFound();

            _branches.Remove(branch);
            return NoContent();
        }

        public static void Main(string[] args)
        {
            // Create an instance of BranchesController to test the methods
            var controller = new BranchesController();

            // Add sample data
            var branch1 = new Branch { BranchId = 1, Name = "Branch 1", Address = "Address 1", City = "City 1", State = "Active", ZipCode = "12345" };
            var branch2 = new Branch { BranchId = 2, Name = "Branch 2", Address = "Address 2", City = "City 2", State = "Active", ZipCode = "67890" };
            controller.Add(branch1);
            controller.Add(branch2);

            // Test Get() method
            var branches = controller.Get();
            foreach (var branch in branches.Value)
            {
                System.Console.WriteLine($"Branch ID: {branch.BranchId}, Name: {branch.Name}");
            }

            // Test Get(id) method
            var branchId = 1;
            var specificBranch = controller.Get(branchId);
            if (specificBranch.Result is OkObjectResult)
            {
                var branch = (specificBranch.Result as OkObjectResult).Value as Branch;
                System.Console.WriteLine($"Branch ID: {branch.BranchId}, Name: {branch.Name}");
            }
        }
    }
}
