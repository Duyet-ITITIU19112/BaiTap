using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace YourNamespace
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            // Create an instance of HttpClient
            using (var httpClient = new HttpClient())
            {
                try
                {
                    // Base URL of the API
                    string baseUrl = "http://localhost:5000";

                    // Make a GET request to /api/branches
                    var response = await httpClient.GetAsync($"{baseUrl}/api/branches");

                    // Check if the response is successful
                    if (response.IsSuccessStatusCode)
                    {
                        // Read the response content as string
                        var responseContent = await response.Content.ReadAsStringAsync();

                        // Display the response
                        Console.WriteLine(responseContent);
                    }
                    else
                    {
                        Console.WriteLine($"Request failed with status code: {response.StatusCode}");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"An error occurred: {ex.Message}");
                }
            }
        }
    }
}
