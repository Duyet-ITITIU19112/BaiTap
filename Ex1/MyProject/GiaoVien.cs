using System;

class GiaoVien : NguoiLaoDong
{
    public double HeSoLuong { get; set; }

    public GiaoVien()
    {
    }

    public GiaoVien(string hoTen, int namSinh, double luongCoBan, double heSoLuong) : base(hoTen, namSinh, luongCoBan)
    {
        HeSoLuong = heSoLuong;
    }

    public new void NhapThongTin(string hoTen, int namSinh, double luongCoBan)
    {
        base.NhapThongTin(hoTen, namSinh, luongCoBan);
    }

    public void NhapThongTin(double heSoLuong)
    {
        HeSoLuong = heSoLuong;
    }

    public override double TinhLuong()
    {
        return LuongCoBan * HeSoLuong * 1.25;
    }

    public override void XuatThongtin()
    {
        base.XuatThongtin();
        Console.WriteLine($"He so luong: {HeSoLuong}, Luong: {TinhLuong()}.");
    }

    public double XuLy()
    {
        return HeSoLuong + 0.6;
    }
}
